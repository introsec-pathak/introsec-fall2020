> Q: Compile and run ```tracer2``` a few times and observe the results.
>
> 1. Where are ```buffer``` and ```array``` relative to stuff?


> 2. What happens to the address of ```stuff``` each time the function recurses?


> 3. What is going on here? Explain why we get these behaviors in (1) and (2) above.


