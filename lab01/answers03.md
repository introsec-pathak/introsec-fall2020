> Compile and run ```tracer3``` and observe the results.
> 
> 1. Each time you run the program, what do all of the code addresses have in common?


> 2. Why does (1) happen? Explain what you're seeing.


> 3. Now re-run the program several times. Do the addresses change from run to run? If so, how do they change each time?


